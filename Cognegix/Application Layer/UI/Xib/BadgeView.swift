//
//  BadgeView.swift
//  
//
//  Created by Suraj Sonawane on 20/03/18.
//  Copyright © 2018 team. All rights reserved.
//

import UIKit

class BadgeView: UIView {

    @IBOutlet weak var imgFirst: UIImageView!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblCountFirst: UILabel!
    
    @IBOutlet weak var imgSec: UIImageView!
    @IBOutlet weak var lblSec: UILabel!
    @IBOutlet weak var lblCountSec: UILabel!
    
    @IBOutlet weak var imgThird: UIImageView!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var lblCountThird: UILabel!
    
}
