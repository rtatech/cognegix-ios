//
//  General.swift//
//
//  Created by Suraj Sonawane on 15/07/17.
//  Copyright ©  . All rights reserved.
//

import UIKit

import NVActivityIndicatorView

class General:NVActivityIndicatorViewable {

   static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
   
    //2017-09-11  >>  11 Sept 2017
    static func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM yyyy"
         if date != nil{
            return  dateFormatter.string(from: date!)
        }else{
            return ""
        }
    }
    
    //14:35:00
    static func convertTimeFormater(_ time: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let fullDate = dateFormatter.date(from: time)
        
        dateFormatter.dateFormat = "hh:mm a"
        
        let time2 = dateFormatter.string(from: fullDate!)
        
        return time2
        
    }


    static func createUserDic(){
        
    }
    
    //TODO: - Save to UserDefaults
    
    //1
    static func saveAny(data: Any, name: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(data, forKey: name)
        defaults.synchronize()
    }
    
 
    
    //2
    static func saveData(data: String, name: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(data, forKey: name)
        defaults.synchronize()
    }
    
    static func loadSaved(name: String) -> String {
        var value = ""
        let defaults = UserDefaults.standard
        if let stringOne = defaults.value(forKey: name) as? String {
            value = stringOne
        }
        return value
    }
    
    static func removeSaved(name: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: name)
    }
   
    
    //TODO: - Activity Indicator Function
    
    static func showActivityIndicator(){
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotate
        //NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor(red: 48/255, green: 63/255, blue: 169/255, alpha: 1.0)
       // NVActivityIndicatorView.DEFAULT_COLOR = UIColor(red: 48/255, green: 63/255, blue: 169/255, alpha: 1.0)
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    static func hideActivityIndicator(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }

}
