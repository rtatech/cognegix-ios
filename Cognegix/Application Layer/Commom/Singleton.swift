//
//  Singleton.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 03/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

final class Singleton {

    // Can't init is singleton
    private init() { }
    
    //MARK: Shared Instance
    static let shared: Singleton = Singleton()
    
    var progDetailImageString : String = String()
    var progDetailTitleString : String = String()
    
    
    var loginCustViewHeight : CGFloat = CGFloat()
    var loginCustViewTop : CGFloat = CGFloat()
    
}
