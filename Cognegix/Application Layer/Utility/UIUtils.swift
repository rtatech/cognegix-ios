//
//  UIUtils.swift
//
//


import Foundation
import UIKit


let SCREEN_WIDTH: Float = Float(UIScreen.main.bounds.size.width)
let SCREEN_HEIGHT: Float = Float(UIScreen.main.bounds.size.height)

let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)


class UIUtils {
    
    
    class func pointsToPixels (_ points: Float)-> CGFloat {
        
        let baseWidth: Float = 320
        let screenWidth: Float = SCREEN_MIN_LENGTH
        
        let sd: CGFloat = CGFloat((points * screenWidth) / baseWidth);
        return sd;
    }
    
    class func alertViewWithMessage(_ title: String, message:String) {
        
        let alertController = UIAlertController(title:title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        
        let action = UIAlertAction(title: NSLocalizedString("OK", comment:""), style: UIAlertActionStyle.default,handler: nil)
        
        alertController.addAction(action)
        let _ = UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }

    
    static func validateBasic(textField: UITextField, name: String, min: Int) -> Bool {
        
        let text: String = textField.text!
        
        if text.characters.count == 0 {
            alertViewWithMessage("", message: "Please enter \(name)!")
            return false
        } else if text.characters.count < min {
            alertViewWithMessage("", message: "\(name) should have least \(min) characters!")
            return false
        }
        
        /*if text.characters.count < min {
            textField.becomeFirstResponder()
            return false
        }*/
        
        return true
    }
    static func validateDropdownTextField(selectedTest: String, name: String) -> Bool {
        
        
        if selectedTest.characters.count == 0 {
            alertViewWithMessage("", message: "Please enter \(name)!")
           return false
        }
//        else if text.characters.count < min {
//            //alertViewWithMessage("", message: "\(name) should have least \(min) characters!")
//            SCLAlertView().showWarning(ALERT_STRING, subTitle: "\(name) should have least \(min) characters!")
//            return false
//        }
        
        /*if text.characters.count < min {
         textField.becomeFirstResponder()
         return false
         }*/
        
        return true
    }

    static func validateEmail(textField: UITextField, name: String) -> Bool {
        
        let text: String = textField.text!
        
        if text.characters.count == 0 {
            textField.becomeFirstResponder()
            alertViewWithMessage("", message: "Please enter \(name)!")
            return false
        }
        
        if !validateEmail(enteredEmail: text) {
            alertViewWithMessage("", message: "Please enter valid \(name)!")
            return false
        }
        
        return true
    }
    
    static func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
}
