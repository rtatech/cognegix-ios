//
//  Strings.swift
//
//
//  Copyright © Suraj. All rights reserved.
//

import Foundation
import UIKit

let BASE_URL = ""
let IMAGE_BASE_URL = ""
let APP_NAME = "Cognegix"

//MACROS

let USER_DEFAULTS = UserDefaults.standard
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
//var SINGLETON_USER_MODEL = FoodBuddiesSingleton.shared.entuserModel

let MAIN_STORYBOARD: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

let NO_NETWORK_CONNECTION = "The Internet connection appears to be offline. Please check your internet connection"
let SERVER_TIME_OUT = "The server is not responding, please try again later."
var appTitle = "Cognegix"

var strNoInternetConnection = "Please check internet connection and try again."
var strSomethigWentWrongRetry = "Something went wrong, Please try again."

//Color


//Notification Observer
let changeControllerToList = "changeControllerToList"

//Color Codes
let menuBgColor = "24a8f3"
let lightGrayColor = "9A9A9A"
let clearColor = ""
let NotificationCountBG = "5A7809"
let btnColor = "0C76B2"

//API Names:
//var pages = "pages"
//var listevents = "listevents"
//var updateregistrationtoken = "updateregistrationtoken"
//var changepassword = "changepassword"
//var detailevent = "detailevent"
//var verifysocialaccount = "verifysocialaccount"
//var createevent = "createevent"
//var bookevent = "bookevent"
//var enquiry = "enquiry"
//var signup_first = "signup_first"
//var postcodeauthenticate = "postcodeauthenticate"

