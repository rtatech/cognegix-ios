//
//  UpcommingActivityViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class UpcommingActivityViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    //TODO: - General
    
    var progress: KDCircularProgress!
    
    var currentCard = ""
    var programTitleArray : [String] = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
    var programImageArray : [String] = ["temp_program_screen.png","temp_program_screen1.jpg","temp_program_screen2.jpg","temp_program_screen3.jpg"]
    
    
    
    //TODO: - Controls
    
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        pageControl.numberOfPages = 4
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        
         //self.createNewShape()
        self.createNewView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom Function
    //TODO: - Custom Function
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    func createNewShape(){
        print("self.customView.frame.size.width:\(self.viwCustom.frame.size.width)")
        print("UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
      //  self.constHeight.constant = UIScreen.main.bounds.size.width
        self.constTop.constant = -(UIScreen.main.bounds.size.width/2.6)
        
        self.viwCustom.layoutIfNeeded()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viwCustom.frame
        rectShape.position = self.viwCustom.center
        rectShape.path = UIBezierPath(roundedRect: self.viwCustom.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .bottomRight], cornerRadii: CGSize(width: self.viwCustom.frame.size.width, height: self.viwCustom.frame.size.width)).cgPath
        
        self.viwCustom.layer.backgroundColor = UIColor(hexString: menuBgColor)?.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.viwCustom.layer.mask = rectShape
    }
    
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return 4
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("LearningProgramView", owner: self, options: nil)![0] as? UIView)! as! LearningProgramView
        viwCard.frame = viwCarousel.frame
        
        viwCard.outerView.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        print("viewForItemAt index:\(index)")
        
        viwCard.imgProgram.image = UIImage(named: self.programImageArray[index])
        viwCard.layer.cornerRadius = viwCard.frame.size.width / 2
        viwCard.clipsToBounds = true
        viwCard.lblProgramTitle.text = programTitleArray[index]
        viwCard.lblProgramObj.text = "identifying a negotiation skills"
        viwCard.btnLaunch.tag = index
        viwCard.btnLaunch.addTarget(self, action: #selector(btnGetCVVPressed), for: .touchUpInside)
        
        viwCard.btnLaunch.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.3
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        self.currentCard = ""
        let index = carousel.currentItemIndex
        print("index:\(index)")
        pageControl.currentPage = index
       
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    //TODO: - UIButton Action

    @objc func btnGetCVVPressed(_ sender: UIButton) {
        let progDtVC = self.storyboard?.instantiateViewController(withIdentifier: "idProgramDetailsViewController") as! ProgramDetailsViewController
        Singleton.shared.progDetailImageString = self.programImageArray[sender.tag]
        Singleton.shared.progDetailTitleString = self.programTitleArray[sender.tag]
        self.navigationController?.pushViewController(progDtVC, animated: true)
    }


}
