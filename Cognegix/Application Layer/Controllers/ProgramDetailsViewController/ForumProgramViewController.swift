//
//  ForumProgramViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 08/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


class ForumTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblDescViwOther: UILabel!
    @IBOutlet weak var lblTitleViwOther: UILabel!
    @IBOutlet weak var lblDescViwMe: UILabel!
    @IBOutlet weak var lblTitleViwMe: UILabel!
    @IBOutlet weak var viwOther: UIView!
    @IBOutlet weak var viwMe: UIView!
    @IBOutlet weak var lblDate: UILabel!
}
class ForumProgramViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    //TODO: - General
    
    //TODO:  - Controls
    
    @IBOutlet weak var btnSendOutlet: UIButton!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var tblMain: UITableView!
    
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //TODO: - UITableviewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ForumTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwMe, clr: lightGrayColor)
        roundedCornerView(viw: cell.viwOther, clr: lightGrayColor)
       
        
        if indexPath.row  == 0{
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = "Marco"
            cell.lblDescViwMe.text = "Lorem Ipsum is simply text of the printing and typesetting."
            cell.viwOther.isHidden = true
            cell.lblDate.text = "21-Mar-2018"
        }else if indexPath.row  == 1{
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = "Finch"
            cell.lblDescViwOther.text = "Lorem Ipsum is simply dummy text of the printing and."
            cell.lblDate.text = ""
        }else if indexPath.row == 2{
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = "Jack"
            cell.lblDescViwOther.text = "Lorem Ipsum is simply dummy text of the and typesetting."
            cell.lblDate.text = ""
            
        }else if indexPath.row  == 3{
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = "Marco"
            cell.lblDescViwMe.text = "Lorem Ipsum is simply dummy text of."
            cell.viwOther.isHidden = true
            cell.lblDate.text = "22-Mar-2018"
        }
        
        return cell
    }
    
    
    //TODO: - UIButton Action
   
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
