//
//  ParticipantListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 07/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ParticipantTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPartLogo: UIImageView!
}

class ParticipantListViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {

    //TODO: - General
    let nameArray : [String] = ["Richard Young","Teresa House","Sebastian Richard"]
    let numberArray : [String] = ["2185753206","9155381904","3137667802"]
    let designationArray : [String] = ["Manager","Software Engineer","CEO"]
    
    //TODO: - Controls
    
    @IBOutlet weak var txtSearch: DesignableUITextField!
    //@IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var tblMain: UITableView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        
        let myLayer = CALayer()
        let myImage = UIImage(named: "bottom_Search")?.cgImage
        myLayer.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        myLayer.contents = myImage
        txtSearch.layer.addSublayer(myLayer)
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.txtSearch.layer.cornerRadius = self.txtSearch.frame.size.height/2
        self.txtSearch.clipsToBounds = true
    }
    
    //TODO: - UITableViewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  self.nameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ParticipantTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerButton(button: cell.btnView, corner: 5, clr: btnColor)
        cell.lblName.text = self.nameArray[indexPath.row]
        cell.lblDesignation.text = self.designationArray[indexPath.row]
        cell.lblMobileNumber.text = "Mobile: " + self.numberArray[indexPath.row]
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(self.profileDetailPress(_:)), for: .touchUpInside)
        return cell
    }
    
    //TODO: - UIButton Action
    @objc func profileDetailPress(_ sender:UIButton){
        let profdtVC = self.storyboard?.instantiateViewController(withIdentifier: "idOtherParticipantProfileViewController") as! OtherParticipantProfileViewController
        self.navigationController?.pushViewController(profdtVC, animated: true)
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
     self.navigationController?.popViewController(animated: true)
    }
}
