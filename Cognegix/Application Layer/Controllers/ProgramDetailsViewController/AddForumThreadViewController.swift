//
//  AddForumThreadViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 16/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class AddForumThreadViewController: BaseVC, UITextViewDelegate {

    
    
    //TODO: - General
     var is_textEdited : Bool = Bool()
    //TODO: - Controls
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var txtQuestion: UITextView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.initPlaceholderForTextView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        self.txtQuestion.delegate = self
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.txtQuestion.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        self.txtQuestion.layer.borderWidth = 1.0
        self.txtQuestion.layer.cornerRadius = 5
        self.txtQuestion.clipsToBounds = true
    }
    
    func initPlaceholderForTextView(){
        
        self.txtQuestion.text = "Enter Subject"
        self.txtQuestion.textColor = UIColor.lightGray
        is_textEdited = false
    }
    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
            is_textEdited = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtQuestion{
            
            if textView.text.isEmpty{
                txtQuestion.text = "Enter Subject"
                txtQuestion.textColor = UIColor.lightGray
                is_textEdited = false
            }
            
        }
    }
    
    
    //TODO: - UIButton Action

    @IBAction func btnPostClick(_ sender: UIButton) {
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
