//
//  ForumThreadListingViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 16/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


class ForumThreadListingTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viwOuter: UIView!
}

class ForumThreadListingViewController: BaseVC, UITableViewDelegate, UITableViewDataSource  {

    //TODO: - General
    let nameArray : [String] = ["Jagat Kumar Gour","Rupal Shetty",]
    let durationArray : [String] = ["30 min","20 min"]
    let subjectArray : [String] = ["Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical","Lorem Ipsum is not simply random text."]
    let descriptionArray : [String] = ["There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
    
    //TODO: - Controls
    
    @IBOutlet weak var viwCircle: UIView!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        circularButton(button: self.btnAddOutlet)
        self.viwCircle.layer.cornerRadius = self.viwCircle.frame.size.width/2
        self.viwCircle.clipsToBounds = true
    }
    
    
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ForumThreadListingTableViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
        cell.imgProfile.image = UIImage(named:"profile_placeholder")
        circularImageView(imgViw: cell.imgProfile)
        cell.lblName.text = self.nameArray[indexPath.row]
        cell.lblSubject.text = self.subjectArray[indexPath.row]
        cell.lblDuration.text = self.durationArray[indexPath.row]
        cell.lblDescription.text = self.descriptionArray[indexPath.row]
        
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Indexpath:\(indexPath.row)")
        let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idForumProgramViewController") as! ForumProgramViewController
        self.navigationController?.pushViewController(forumVC, animated: true)
    }
    
    //TODO: - UIButton Action

    @IBAction func btnAddNewClick(_ sender: UIButton) {
        let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idAddForumThreadViewController") as! AddForumThreadViewController
        self.navigationController?.pushViewController(forumVC, animated: true)
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
