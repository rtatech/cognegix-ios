//
//  ProgramDetailsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 03/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

import AVKit
import AVFoundation

class ProgramDetailsViewController: BaseVC {

    //TODO: - General
    
    //TODO: - Controls
     var player: AVPlayer?
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var viwMessageBoard: UIView!
    @IBOutlet weak var btnParticipantOutlet: UIButton!
    @IBOutlet weak var btnForumOutlet: UIButton!
    @IBOutlet weak var viwRating: HCSStarRatingView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwVideo: UIView!
    @IBOutlet weak var btnViewDetailsOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.initialise()
        self.initPlayer()
        //self.initializeVideoPlayerWithVideo()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        player?.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom function
    func initialise(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        
        roundedCornerButton(button: self.btnViewDetailsOutlet, corner: 5, clr: btnColor)
        roundedCornerButton(button: self.btnForumOutlet, corner: 5, clr: btnColor)
        roundedCornerButton(button: self.btnParticipantOutlet, corner: 5, clr: btnColor)
        roundedCornerView(viw: self.viwMessageBoard, clr: lightGrayColor)
        
        viwCustom.round(corners: [.topRight, .bottomRight], radius: self.viwCustom.frame.size.height/2)
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    //TODO: - Custom function
    func initPlayer(){
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.viwVideo.bounds
        self.viwVideo.layer.addSublayer(playerLayer)
        player.play()
    }
    func initializeVideoPlayerWithVideo() {
        
        // get the path string for the video from assets
        let videoString:String? = Bundle.main.path(forResource: "video", ofType: "mp4")
        guard let unwrappedVideoPath = videoString else {return}
        
        // convert the path string to a url
        let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
        
        // initialize the video player with the url
        self.player = AVPlayer(url: videoUrl)
        
        
        // create a video layer for the player
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        
        // make the layer the same size as the container view
        layer.frame = viwVideo.bounds
        
        // make the video fill the layer as much as possible while keeping its aspect size
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        // add the layer to the container view
        viwVideo.layer.addSublayer(layer)
        
        
        player?.play()
    }
    

    //TODO: - UIButton Action
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Home display")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1003:
           // let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
           // helpVC.isMenuNavigation = false
           // self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Forum : 61, participant: 62, Learning Tree: 63
    @IBAction func btnClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 61:
            let forumVC = self.storyboard?.instantiateViewController(withIdentifier: "idForumThreadListingViewController") as! ForumThreadListingViewController
            self.navigationController?.pushViewController(forumVC, animated: true)
            break;
        case 62:
           let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idParticipantListViewController") as! ParticipantListViewController
           self.navigationController?.pushViewController(partVC, animated: true)
            break;
        case 63:
            let partVC = self.storyboard?.instantiateViewController(withIdentifier: "idLearningTreeViewController") as! LearningTreeViewController
            self.navigationController?.pushViewController(partVC, animated: true)
            break;
        default:
            break;
        }
    }
    
}
