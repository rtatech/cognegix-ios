//
//  MessageBoardViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class MessageBoardViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    //TODO: - General
    
     var subjectArray : [String] = ["New program \"Understanding of Negotiation Skills\" is about to start this week.","Virtual classroom is scheduled on 25th March 2018."]
    
    //TODO: - Controls
    
    @IBOutlet weak var viwIcon: UIView!
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viwCarousel: iCarousel!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageControl.numberOfPages = subjectArray.count
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        self.initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        viwIcon.round(corners: [.topRight, .bottomRight], radius: self.viwIcon.frame.size.height/2)
        roundedCornerView(viw: self.viwOuter, clr: lightGrayColor)
    }
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return subjectArray.count
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("MessageBoardView", owner: self, options: nil)![0] as? UIView)! as! MessageBoardView
        viwCard.frame = viwCarousel.frame
        
        viwCard.outerView.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        
        viwCard.lblDateVal.text = "10th March 2018"
        viwCard.lblSenderVal.text = "Facilitator"
        viwCard.lblSubjectVal.text = self.subjectArray[index]
        
        viwCard.btnViewOutlet.tag = index
        viwCard.btnViewOutlet.addTarget(self, action: #selector(btnViewClick), for: .touchUpInside)
        
        viwCard.btnViewOutlet.layer.cornerRadius = 5
        viwCard.btnViewOutlet.clipsToBounds = true
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        pageControl.currentPage = index
        
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    

    //TODO: - UIButton Action

    @objc func btnViewClick(_ sender: UIButton) {
        let commonWBVC = self.storyboard?.instantiateViewController(withIdentifier: "idMessageBoardDetailsViewController") as! MessageBoardDetailsViewController
        self.navigationController?.pushViewController(commonWBVC, animated: true)
        
    }
    
}
