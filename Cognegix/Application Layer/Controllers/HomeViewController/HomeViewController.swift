//
//  HomeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class HomeViewController: BaseVC {

    
    //TODO: - General
    
    
    //TODO: - Contorls
    
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnProfileOutlet: UIButton!
    @IBOutlet weak var btnNotificationOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var menuOutlet: UIButton!
    
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()
        self.autolayoutCustomView()
        print("Hello Home viewcontroller")
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.initialise()
        self.createBottomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom function
    func initialise(){
        circularButton(button: self.btnProfileOutlet)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
      
    }
    
    func autolayoutCustomView(){
        print("APP_DELEGATE.screenWidth:\(APP_DELEGATE.screenWidth)")
         if APP_DELEGATE.screenWidth > 414{
            //iPad air 2
           // constTop.constant = -350//(APP_DELEGATE.screenWidth/1.5)
            constHeight.constant = APP_DELEGATE.screenWidth/1.2
          
        }
        self.viwCustom.layoutIfNeeded()
    }
    
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
   
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Home display")
            break;
        case 1002:
             showAlert(strMSG: "Search display")
            break;
        case 1003:
            //let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
            //helpVC.isMenuNavigation = false
            //self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    

}
