//
//  ActivityCalenderViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 28/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ActivityCalenderViewController: BaseVC,iCarouselDelegate, iCarouselDataSource {

    //TODO: - General
    
    //TODO: - Controls
    
    @IBOutlet weak var pageControlNew: UIPageControl!
    @IBOutlet weak var viwCarouselNew: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viwDetail: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var viwOuter: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Temp Calender function
        var tenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 10, to: Date(), options: [])!
            
        }
        print(tenDaysfromNow.dayOfWeek()!) // Wednesday
        
        print(tenDaysfromNow)
        
        
        
        
        //Carousel
        
        pageControl.numberOfPages = 4
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        pageControlNew.numberOfPages = 2
        pageControlNew.hidesForSinglePage = true
        viwCarouselNew.bounces = false
        viwCarouselNew.delegate = self
        viwCarouselNew.dataSource = self
        viwCarouselNew.reloadData()
        
       roundedCornerView(viw: viwDetail, clr: btnColor)
        
        //Corner radious
        self.viwOuter.layer.cornerRadius = 5
        self.viwOuter.clipsToBounds = true
        self.viwOuter.layer.borderWidth = 1
        self.viwOuter.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        if carousel == viwCarousel{
        return 4
        }else {
            return 2
        }
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        if carousel == viwCarousel{
        let viwCard = (Bundle.main.loadNibNamed("CalenderView", owner: self, options: nil)![0] as? UIView)! as! CalenderView
        viwCard.frame = viwCarousel.frame
        
        viwCard.outerView.layer.cornerRadius = 5
        viwCard.clipsToBounds = true
        print("viewForItemAt index:\(index)")
        
        
        viwCard.lblFirstTitle.text = "Sat"
        viwCard.lblTwoTitle.text = "Sun"
        viwCard.lblThreeTitle.text = "Mon"
        viwCard.lblFourTitle.text = "Tue"
        viwCard.lblFiveTitle.text = "Wed"
        viwCard.lblSixTitle.text = "Thu"
        viwCard.lblSevenTitle.text = "Fri"
        
        
        if index == 0{
            viwCard.lblFirstDate.text = "1"
            viwCard.lblTwoDate.text = "2"
            viwCard.lblThreeDate.text = "3"
            viwCard.lblFourDate.text = "4"
            viwCard.lblFiveDate.text = "5"
            viwCard.lblSixDate.text = "6"
            viwCard.lblSevenDate.text = "7"
            
            viwCard.btnFirstOutlet.tag = 1
            viwCard.btnTwoOutlet.tag = 2
            viwCard.btnThreeOutlet.tag = 3
            viwCard.btnFourOutlet.tag = 4
            viwCard.btnFiveOutlet.tag = 5
            viwCard.btnSixOutlet.tag = 6
            viwCard.btnSevenOutlet.tag = 7
            
            viwCard.lblFirstIndicator.isHidden = true
            viwCard.lblTwoIndicator.isHidden = true
            viwCard.lblThreeIndicator.isHidden = false
            viwCard.lblFourIndicator.isHidden = true
            viwCard.lblFiveIndicator.isHidden = true
            viwCard.lblSixIndicator.isHidden = true
            viwCard.lblSevenIndicator.isHidden = false
        }else if index == 1{
            viwCard.lblFirstDate.text = "8"
            viwCard.lblTwoDate.text = "9"
            viwCard.lblThreeDate.text = "10"
            viwCard.lblFourDate.text = "11"
            viwCard.lblFiveDate.text = "12"
            viwCard.lblSixDate.text = "13"
            viwCard.lblSevenDate.text = "14"
            
            viwCard.btnFirstOutlet.tag = 8
            viwCard.btnTwoOutlet.tag = 9
            viwCard.btnThreeOutlet.tag = 10
            viwCard.btnFourOutlet.tag = 11
            viwCard.btnFiveOutlet.tag = 12
            viwCard.btnSixOutlet.tag = 13
            viwCard.btnSevenOutlet.tag = 14
            
            viwCard.lblFirstIndicator.isHidden = true
            viwCard.lblTwoIndicator.isHidden = false
            viwCard.lblThreeIndicator.isHidden = true
            viwCard.lblFourIndicator.isHidden = false
            viwCard.lblFiveIndicator.isHidden = true
            viwCard.lblSixIndicator.isHidden = false
            viwCard.lblSevenIndicator.isHidden = true
        }else if index == 2{
            viwCard.lblFirstDate.text = "15"
            viwCard.lblTwoDate.text = "16"
            viwCard.lblThreeDate.text = "17"
            viwCard.lblFourDate.text = "18"
            viwCard.lblFiveDate.text = "19"
            viwCard.lblSixDate.text = "20"
            viwCard.lblSevenDate.text = "21"
            viwCard.btnFirstOutlet.tag = 15
            viwCard.btnTwoOutlet.tag = 16
            viwCard.btnThreeOutlet.tag = 17
            viwCard.btnFourOutlet.tag = 18
            viwCard.btnFiveOutlet.tag = 19
            viwCard.btnSixOutlet.tag = 20
            viwCard.btnSevenOutlet.tag = 21
            
            viwCard.lblFirstIndicator.isHidden = true
            viwCard.lblTwoIndicator.isHidden = true
            viwCard.lblThreeIndicator.isHidden = true
            viwCard.lblFourIndicator.isHidden = true
            viwCard.lblFiveIndicator.isHidden = true
            viwCard.lblSixIndicator.isHidden = false
            viwCard.lblSevenIndicator.isHidden = true
            
        }else if index == 3{
            viwCard.lblFirstDate.text = "22"
            viwCard.lblTwoDate.text = "23"
            viwCard.lblThreeDate.text = "24"
            viwCard.lblFourDate.text = "25"
            viwCard.lblFiveDate.text = "26"
            viwCard.lblSixDate.text = "27"
            viwCard.lblSevenDate.text = "28"
            
            viwCard.btnFirstOutlet.tag = 22
            viwCard.btnTwoOutlet.tag = 23
            viwCard.btnThreeOutlet.tag = 24
            viwCard.btnFourOutlet.tag = 25
            viwCard.btnFiveOutlet.tag = 26
            viwCard.btnSixOutlet.tag = 27
            viwCard.btnSevenOutlet.tag = 28
            
            viwCard.lblFirstIndicator.isHidden = true
            viwCard.lblTwoIndicator.isHidden = false
            viwCard.lblThreeIndicator.isHidden = true
            viwCard.lblFourIndicator.isHidden = false
            viwCard.lblFiveIndicator.isHidden = true
            viwCard.lblSixIndicator.isHidden = true
            viwCard.lblSevenIndicator.isHidden = true
        }
        
        
        viwCard.btnFirstOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnTwoOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnThreeOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnFourOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnFiveOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnSixOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
        viwCard.btnSevenOutlet.addTarget(self, action: #selector(btnDateSelect), for: .touchUpInside)
             return viwCard
        }else if carousel == viwCarouselNew{
            let viwCard = (Bundle.main.loadNibNamed("CalenderDetailView", owner: self, options: nil)![0] as? UIView)! as! CalenderDetailView
            
            viwCard.frame = viwCarouselNew.frame
            
            viwCard.btnLaunch.layer.cornerRadius = 5
            viwCard.btnLaunch.clipsToBounds = true
            viwCard.lblTitle.text = "Assessment administration application"
             return viwCard
        }
        
        let viwCard = (Bundle.main.loadNibNamed("CalenderView", owner: self, options: nil)![0] as? UIView)! as! CalenderView
        viwCard.frame = viwCarousel.frame
        return viwCard
        
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        print("index:\(index)")
        if carousel == viwCarousel{
        pageControl.currentPage = index
        }else{
            pageControlNew.currentPage = index
        }
        
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }

    
    //TODO: - UIButton
    
    @objc func btnDateSelect(_ sender: UIButton) {
        self.lblDate.text = "\(sender.tag)"
    }

}
