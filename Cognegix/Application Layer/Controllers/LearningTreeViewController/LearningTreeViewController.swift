//
//  LearningTreeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class LearningTreeViewController: BaseVC, UIWebViewDelegate{
    
    
    //TODO: - General
    
    //TODO: - Controls
    
    
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var viwWeb: UIWebView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Bottom Menu Navigation
        self.createBottomView()
        
        
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        let url = URL (string: "http://cogvc.testursites.info/cgx-front/MLT_5_19")
        let requestObj = URLRequest(url: url!)
        viwWeb.delegate = self
        viwWeb.loadRequest(requestObj)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UIWebView Delegate Method implemenation
    func webViewDidStartLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action
    //TODO: - UIButton Action
    
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Show home")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1003:
            //let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
            // helpVC.isMenuNavigation = false
            // self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
