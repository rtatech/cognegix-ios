//
//  LoginViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class LoginViewController: BaseVC {

    //TODO:- General
    var tapGesture : UITapGestureRecognizer  = UITapGestureRecognizer()
    
    //TODO: - Controls
    //CustomView
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constInputField: NSLayoutConstraint!
    @IBOutlet weak var constLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var constLogoWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viwLogoBack: UIView!
    
    @IBOutlet weak var lblTerms: UILabel!
    
    @IBOutlet weak var btnForgotPwdOutlet: UIButton!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var imgBackground: UIImageView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.autolayoutCustomView()
        
       
       self.createNewView()
        
        circularImageView(imgViw: self.imgLogo)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
         self.initialization()
        circularTextfield(textfield: self.txtUsername)
        circularTextfield(textfield: self.txtPassword)
        circularButton(button: self.btnLoginOutlet)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - Custom Function
    func initialization(){
        self.btnForgotPwdOutlet.setTitleColor(UIColor(hexString: menuBgColor), for: .normal)
        //Left padding
        self.txtPassword.setLeftPaddingPoints(20)
        self.txtUsername.setLeftPaddingPoints(20)
        //Right padding
        self.txtPassword.setRightPaddingPoints(8)
        self.txtUsername.setRightPaddingPoints(8)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.tapLabel(_:)))
        self.lblTerms.addGestureRecognizer(tapGesture)
        self.lblTerms.isUserInteractionEnabled = true
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.linkLabel()
        
      
    }
    
    func linkLabel(){
        self.lblTerms.text = "By logging in, you agree to the Terms & Conditions of Cognegix Digital Learning Redefined"
        let text = (self.lblTerms.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms & Conditions")
        underlineAttriString.addAttribute(NSAttributedStringKey.font, value:UIFont(name: "HelveticaNeue-bold", size: 13)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value:NSUnderlineStyle.styleSingle.rawValue, range: range1)
         self.lblTerms.attributedText = underlineAttriString
    }
    func autolayoutCustomView(){
        print("APP_DELEGATE.screenWidth:\(APP_DELEGATE.screenWidth)")
        if APP_DELEGATE.screenWidth == 320{
            constTop.constant = -80
            constHeight.constant = 320
            constInputField.constant = 32
        }else if APP_DELEGATE.screenWidth == 375{
            constTop.constant = -100
            constHeight.constant = 375
            constInputField.constant = 46
        }else if APP_DELEGATE.screenWidth == 414{
            
        }else if APP_DELEGATE.screenWidth < 320{
            
        }else if APP_DELEGATE.screenWidth > 414{
            //iPad air 2
            constTop.constant = -350//(APP_DELEGATE.screenWidth/1.5)
            constHeight.constant = APP_DELEGATE.screenWidth
            constInputField.constant = 58
            constLogoWidth.constant = 350
             constLogoHeight.constant = 350
        }
         self.viwCustom.layoutIfNeeded()
    }
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    
    func createNewShape(){
        print("self.customView.frame.size.width:\(self.viwCustom.frame.size.width)")
        print("UIScreen.main.bounds.size.width:\(UIScreen.main.bounds.size.width)")
        self.constHeight.constant = UIScreen.main.bounds.size.width
        self.constTop.constant = -(UIScreen.main.bounds.size.width/4)
        
        self.viwCustom.layoutIfNeeded()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.viwCustom.frame
        rectShape.position = self.viwCustom.center
        rectShape.path = UIBezierPath(roundedRect: self.viwCustom.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .bottomRight], cornerRadii: CGSize(width: self.viwCustom.frame.size.width, height: self.viwCustom.frame.size.width)).cgPath
        
        self.viwCustom.layer.backgroundColor = UIColor(hexString: menuBgColor)?.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.viwCustom.layer.mask = rectShape
    }
    
    //TODO: - UIButton Action
    //11: Forgot, 12: Login
    
    
    @IBAction func btnClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 11:
            //Forgot Pwd button action
            let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "idForgotPasswordFirstViewController")  as! ForgotPasswordFirstViewController
            self.present(forgotVC, animated: true, completion: nil)
            break;
        case 12:
            //Login button action
            let tempVc = self.storyboard?.instantiateViewController(withIdentifier: "idRevelBase")  as! UINavigationController
            self.present(tempVc, animated: true, completion: nil)
            
            break;
        default:
            break;
        }
    }
    
    @IBAction func tapLabel(_ gesture: UITapGestureRecognizer) {
        let text = (self.lblTerms.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: termsRange) {
            let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "idTermsAndConditionsViewController")  as! TermsAndConditionsViewController
            self.present(termsVC, animated: true, completion: nil)
        } else if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: privacyRange) {
            print("Tapped privacy")
        } else {
            print("Tapped none")
        }
    }
    
    

}
