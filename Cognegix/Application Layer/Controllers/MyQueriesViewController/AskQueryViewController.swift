//
//  AskQueryViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 11/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class AskQueryViewController: BaseVC , UITextViewDelegate {
    
    
    //TODO: - General
    let dropDown = DropDown()
    
    
    
    var is_textEdited : Bool = Bool()
    //TODO: - Controls
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var lblDDPerson: UILabel!
    @IBOutlet weak var lblDDTopic: UILabel!
    @IBOutlet weak var lblDDProgram: UILabel!
    @IBOutlet weak var txtQuestion: UITextView!
    @IBOutlet weak var btnPersonToAskOutlet: UIButton!
    @IBOutlet weak var btnTopicOutlet: UIButton!
    @IBOutlet weak var btnProgramOutlet: UIButton!
    //TODO: - Let's Code
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.design()
        initPlaceholderForTextView()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func design(){
        roundedCornerButton(button: self.btnProgramOutlet, corner: 5, clr: lightGrayColor)
        roundedCornerButton(button: self.btnTopicOutlet, corner: 5, clr: lightGrayColor)
        roundedCornerButton(button: self.btnPersonToAskOutlet, corner: 5, clr: lightGrayColor)
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.txtQuestion.layer.cornerRadius = 5
        self.txtQuestion.clipsToBounds = true
        self.txtQuestion.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        self.txtQuestion.layer.borderWidth  = 1
        self.txtQuestion.delegate = self
        
    }
    
    
    func initPlaceholderForTextView(){
        
        
        self.txtQuestion.text = "Enter Question"
        self.txtQuestion.textColor = UIColor.lightGray
        is_textEdited = false
        
    }
    
    //TODO: UITextViewDelegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray{
            textView.text = nil
            textView.textColor = UIColor.black
            is_textEdited = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtQuestion{
            
            if textView.text.isEmpty{
                txtQuestion.text = "Enter Question"
                txtQuestion.textColor = UIColor.lightGray
                is_textEdited = false
            }
            
        }
    }
    
    
    //TODO: - UIButton Action
    
    //Tag 71: Program, 72: Topic, 73: Person to Ask
    @IBAction func btnDropDownClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 71:
            dropDown.anchorView = lblDDProgram
            dropDown.dataSource = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.btnProgramOutlet.setTitle(item, for: .normal)
                print("Selected item: \(item) at index: \(index)")
            }
            dropDown.show()
            break;
        case 72:
            dropDown.anchorView = lblDDTopic
            dropDown.dataSource = ["Negotiation Skills", "Communication Skills", "English Training"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.btnTopicOutlet.setTitle(item, for: .normal)
            }
            
            dropDown.show()
            break;
        case 73:
            dropDown.anchorView = lblDDPerson
            dropDown.dataSource = ["Admin", "Facilitator", "Student"]
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.btnPersonToAskOutlet.setTitle(item, for: .normal)
                print("Selected item: \(item) at index: \(index)")
            }
            
            dropDown.show()
            break;
        default:
            break;
        }
    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        if  self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}
