//
//  QueryChatViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 11/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


class QueryChatViewCell : UITableViewCell{
    
    @IBOutlet weak var lblDescViwOther: UILabel!
    @IBOutlet weak var lblTitleViwOther: UILabel!
    @IBOutlet weak var lblDescViwMe: UILabel!
    @IBOutlet weak var lblTitleViwMe: UILabel!
    @IBOutlet weak var viwOther: UIView!
    @IBOutlet weak var viwMe: UIView!
    @IBOutlet weak var lblDate: UILabel!
}
class QueryChatViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    
    //TODO: - General
    var dateArray : [String] = ["20-Mar-2018","","21-Mar-2018",""]
    //TODO: - Controls
    
    @IBOutlet weak var lblProgTitle: UILabel!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSendOutlet: UIButton!
    
    //TODO: - Let's Message
    
    override func viewDidLoad() {
        super.viewDidLoad()
      lblProgTitle.text = "Negotiation Skill"
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.tblMain.tableFooterView = UIView()
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UITableviewDataSource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! QueryChatViewCell
        cell.selectionStyle = .none
        
        roundedCornerView(viw: cell.viwMe, clr: lightGrayColor)
        roundedCornerView(viw: cell.viwOther, clr: lightGrayColor)
        
        if indexPath.row % 2 == 0{
            cell.viwMe.isHidden = false
            cell.lblTitleViwMe.text = "Marco"
            cell.lblDescViwMe.text = "Lorem Ipsum is simply dummy text of the printing"
            cell.viwOther.isHidden = true
            cell.lblDate.text = self.dateArray[indexPath.row]
        }else{
            cell.viwMe.isHidden = true
            cell.viwOther.isHidden = false
            cell.lblTitleViwOther.text = "Jack"
            cell.lblDescViwOther.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            cell.lblDate.text = self.dateArray[indexPath.row]
        }
        return cell
    }
    
    
    @IBAction func btnSendClick(_ sender: UIButton) {
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
