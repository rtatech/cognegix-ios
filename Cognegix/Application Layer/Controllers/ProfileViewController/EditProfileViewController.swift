//
//  EditProfileViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 07/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseVC, UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    
    
    //TODO: - General
    let imgPicker : UIImagePickerController = UIImagePickerController()
    var is_imageUpload : Bool = Bool()
    var is_profileImageSelected : Bool = Bool()
    
    //TODO: - Controls
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCompanyName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFb: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTwitter: SkyFloatingLabelTextField!
    @IBOutlet weak var txtGPlus: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLinkedIn: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSecondEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var viwCustom: UIView!
    
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.autolayoutCustomView()
        self.createNewView()
        
        self.initialise()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom function
    func initialise(){
        circularButton(button: self.btnSaveOutlet)
        circularImageView(imgViw: self.imgProfile)
        
        self.imgProfile.layer.borderColor = UIColor.white.cgColor
        self.imgProfile.layer.borderWidth = 5
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
        
        self.btnSaveOutlet.layer.borderColor = UIColor.white.cgColor
        self.btnSaveOutlet.layer.borderWidth = 5
        self.btnSaveOutlet.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        
        self.txtFirstName.text = "Marco"
        self.txtLastName.text = "Fernandez"
        self.txtDesignation.text = "CEO & Co-Founder"
        self.txtCompanyName.text = "Company Name"
        self.txtFb.text = "http://www.facebook.com/marco.fernandez"
        txtTwitter.text = "https://twitter.com/marco.fernandez"
        txtGPlus.text = "https://plus.google.com/marco.fernandez"
        txtLinkedIn.text = "https://www.linkedin.com/marcofernandez"
        txtEmail.text = "marco.fernandez@email.com"
        txtSecondEmail.text = "marco.fernandez.second@email.com"
        txtPhoneNumber.text = "8001009009"
    }
    
    func autolayoutCustomView(){
        
        if APP_DELEGATE.screenWidth == 320{
            constTop.constant = -80
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 375{
            constTop.constant = -60
            constHeight.constant = 300
            
        }else if APP_DELEGATE.screenWidth == 414{
            
        }else if APP_DELEGATE.screenWidth < 320{
            
        }
        self.viwCustom.layoutIfNeeded()
    }
    
    func createNewView(){
        viwCustom.backgroundColor = UIColor(hexString: menuBgColor)
        viwCustom.round(corners: [.bottomLeft, .bottomRight], radius: self.viwCustom.frame.size.width/2)
        self.viwCustom.layoutIfNeeded()
    }
    
    
    //TODO: - Custom Function
    func askToChangeProfileImage(){
        let alert = UIAlertController(title: "Let's get a picture", message: "Choose a Picture Method", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        self.imgPicker.delegate = self
        
        if(is_imageUpload){
            let removeImageButton = UIAlertAction(title: "Remove profile picture", style: UIAlertActionStyle.destructive) { (UIAlertAction) -> Void in
                self.is_imageUpload = false
                self.imgProfile.image = UIImage(named: "profile_placeholder")
            }
            alert.addAction(removeImageButton)
        }else{
            print("No image is selected")
        }
        
        //Add AlertAction to select image from library
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imgPicker.delegate = self
            self.imgPicker.allowsEditing = true
            self.is_profileImageSelected = true
            self.present(self.imgPicker, animated: true, completion: nil)
        }
        
        //Check if Camera is available, if YES then provide option to camera
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
                self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imgPicker.allowsEditing = true
                self.is_profileImageSelected = true
                self.present(self.imgPicker, animated: true, completion: nil)
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
            
        }
        
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (UIAlertAction) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    // TODO: - UIImagePickerDelegate Methods
    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let pickedImage : UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // if(self.is_profileImageSelected){
        
        //let imageData = UIImageJPEGRepresentation(pickedImage, 0.8)
        self.imgProfile.image = pickedImage
        is_imageUpload = true
        
        // }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        if(self.is_profileImageSelected){
            is_imageUpload = false
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    //TODO: - UIButton Action
    
    @IBAction func btnEditProfileClick(_ sender: UIButton) {
        self.askToChangeProfileImage()
    }
    
    
    @IBAction func btnSaveClick(_ sender: UIButton) {
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSwitchChanged(_ sender: Any) {
    }
}
