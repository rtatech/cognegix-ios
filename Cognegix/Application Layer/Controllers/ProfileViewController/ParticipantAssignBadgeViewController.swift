//
//  ParticipantAssignBadgeViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ParticipantAssignTableViewCell : UITableViewCell{
    @IBOutlet weak var lblBadge: UILabel!
}


class ParticipantAssignBadgeViewController: BaseVC {
    
    //TODO: - General2018
    var valueArray : [String] = []
    //TODO: - Controls
    
    @IBOutlet weak var btnCancelOutlet: UIButton!
    @IBOutlet weak var btnAssignOutlet: UIButton!
    @IBOutlet weak var viwBlur: UIView!
    @IBOutlet weak var viwAlert: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var lblCurrentLimit: UILabel!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ParticipantAssignBadgeViewController.handleTap(_:)))
        
        viwBlur.isUserInteractionEnabled = true
        viwBlur.addGestureRecognizer(tap)
        
        self.tblMain.tableFooterView = UIView()
        
        self.btnCancelOutlet.layer.cornerRadius = 5
        self.btnCancelOutlet.clipsToBounds = true
        self.btnAssignOutlet.layer.cornerRadius = 5
        self.btnAssignOutlet.clipsToBounds = true
        
        self.viwAlert.layer.cornerRadius = 5
        self.viwAlert.clipsToBounds = true
        self.lblCurrentLimit.text = "Current Limit: 20"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        // handling code
        print("Tapped")
        self.dismiss(animated: false, completion: nil)
    }
    
    
    //TAg: 11: Assign, 12: Cancel
    @IBAction func btnActionClick(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 11:
            self.dismiss(animated: false, completion: nil)
            break;
        case 12:
            self.dismiss(animated: false, completion: nil)
            break;
        default:
            break;
        }
    }
    
}
