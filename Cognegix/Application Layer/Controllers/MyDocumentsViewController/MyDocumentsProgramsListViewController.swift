//
//  MyDocumentsProgramsListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class MyDocumentsProgramListTableViewCell : UITableViewCell{
    
    @IBOutlet weak var lblIndicator: UILabel!
    @IBOutlet weak var lblProgramTitle: UILabel!
}

class MyDocumentsProgramsListViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    //TODO: - General
    var titleArray = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
    
    //TODO: - Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var menuBtnOutlet: UIButton!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        //Bottom Menu Navigation
        self.createBottomView()
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
//        self.tblMain.rowHeight = UITableViewAutomaticDimension
//        self.tblMain.estimatedRowHeight = 100;
//        
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuBtnOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyDocumentsProgramListTableViewCell
        
        cell.selectionStyle = .none
        cell.lblProgramTitle.text = self.titleArray[indexPath.row]
        cell.lblIndicator.text = "PR"
        cell.lblIndicator.backgroundColor = .random()
        cell.lblIndicator.layer.cornerRadius = cell.lblIndicator.frame.size.width/2
        cell.lblIndicator.clipsToBounds = true
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dtVC = self.storyboard?.instantiateViewController(withIdentifier: "idMyDocumentListViewController") as! MyDocumentListViewController
        self.navigationController?.pushViewController(dtVC, animated: true)
    }

    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action

    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
           showAlert(strMSG: "Show home")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1003:
            //let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
           // helpVC.isMenuNavigation = false
           // self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
}
