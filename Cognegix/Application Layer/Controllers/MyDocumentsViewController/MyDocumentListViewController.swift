//
//  MyDocumentListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class MyDocumentListTableViewCell : UITableViewCell{
    
    @IBOutlet weak var imgBadge: UIImageView!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwOuter: UIView!
}


class MyDocumentListViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    //TODO: - General
    var titleArray : [String] = ["Understanding a negotiation situation","Understanding a negotiation situation","Understanding a negotiation situation","Understanding a negotiation situation","Understanding a negotiation situation"]
    
    var completionDateArray : [String] = ["Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018"]
    
    let dropDown = DropDown()
    
    //TODO: - Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var tblMain: UITableView!
    
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var lblDDProgram: UILabel!
    @IBOutlet weak var btnSelectProgramOutlet: UIButton!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var btnBackOutlet: UIButton!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //This function is used to init
    func initialise(){
        self.btnSelectProgramOutlet.layer.cornerRadius = 5
        self.btnSelectProgramOutlet.clipsToBounds = true
        self.btnSelectProgramOutlet.layer.borderColor = UIColor.black.cgColor
        self.btnSelectProgramOutlet.layer.borderWidth  = 1
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.tblMain.tableFooterView = UIView()
        self.tblMain.dataSource = self
        self.tblMain.delegate = self
    }
    
    
    //TODO: - UITableViewDatasource method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        print(" titleArray.count:\( titleArray.count)")
         return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyDocumentListTableViewCell
        
        //Style
        cell.selectionStyle = .none
        
        cell.viwOuter.layer.cornerRadius = 5
        cell.viwOuter.clipsToBounds = true
        cell.viwOuter.layer.borderWidth = 1
        cell.viwOuter.layer.borderColor = UIColor.lightGray.cgColor
       
        
        cell.lblTitle.text = self.titleArray[indexPath.row]
       // cell.lblDate.text = self.completionDateArray[indexPath.row]
        
        cell.viwCustom.layer.cornerRadius = cell.viwCustom.frame.size.height/2
        cell.viwCustom.clipsToBounds = true
       
        
        cell.btnView.layer.cornerRadius = 5
        cell.btnView.clipsToBounds = true
        cell.btnView.addTarget(self, action: #selector(handleShare(_:)), for: .touchUpInside)
        return cell
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }

    //TODO: - UIButton Action
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Home display")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1003:
           // let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
           // helpVC.isMenuNavigation = false
           // self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
           displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    @objc func handleShare(_ sender: UIButton){
        //displayShareSheet(strBtnTitle:"Download")
    }

    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectProgClick(_ sender: UIButton) {
        dropDown.anchorView = lblDDProgram
        dropDown.dataSource = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectProgramOutlet.setTitle(item, for: .normal)
            print("Selected item: \(item) at index: \(index)")
        }
        dropDown.show()
        
    }
}
