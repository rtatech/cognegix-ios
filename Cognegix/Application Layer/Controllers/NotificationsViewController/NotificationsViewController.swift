//
//  NotificationsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit


class NotificationsTableViewCell : UITableViewCell{
    
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblNotificationMsg: UILabel!
}

class NotificationsViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    
    //TODO: - General
     let notificationTitleArray : [String] = ["New program Understanding of \"Negotiation Skills\" is about to start this week.","You have successfully completed topic \"Identifying objectives and building agendas\""]
    //TODO: - Controls
    
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var btnProfileOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.tblMain.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //circularButton(button: self.btnProfileOutlet)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return notificationTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! NotificationsTableViewCell
        cell.selectionStyle = .none
        
        cell.lblDuration.text = "1 day ago"
        cell.lblNotificationMsg.text = self.notificationTitleArray[indexPath.row]
        
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let notiDt = self.storyboard?.instantiateViewController(withIdentifier: "idNotificationDetailsViewController") as! NotificationDetailsViewController
        self.navigationController?.pushViewController(notiDt, animated: true)
    }
    

    @IBAction func btnBackClick(_ sender: UIButton) {
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
