//
//  NotificationDetailsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 13/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class NotificationDetailsViewController: BaseVC {

    
    //TODO: - General
    
    //TODO: - Controls
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
