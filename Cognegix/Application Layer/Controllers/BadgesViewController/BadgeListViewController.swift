//
//  BadgeListViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 29/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class BadgeListViewController: BaseVC, iCarouselDelegate, iCarouselDataSource, UITableViewDelegate, UITableViewDataSource{

    
    //TODO: - General
    var titleArray : [String] = ["Fastest Completion","Faculty Appreciation","Participant Appreciation","Content Rating","Quiz Completion"]
    var badgeArray : [String] = ["badge1","badge2","badge3","badge4","badge5"]
    
    var completionDateArray : [String] = ["Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018","Completion Date: 21 Jan 2018"]
    
    var leaderBoardNameArray : [String] = ["Rahulkumar Sharma","Rakesh Sharma"]
    var leaderBoardBadgeArray : [String] = ["Fastest Completion","Faculty Appreciation"]
     var leaderBoardBadgeImageArray : [String] = ["badge1","badge2"]
    
    //TODO: - Controls
    let dropDown = DropDown()
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var viwCustom: UIView!
    @IBOutlet weak var viwCarousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var constLeaderboard: NSLayoutConstraint!
    @IBOutlet weak var viwLeaderboard: UIView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var lblDDProgram: UILabel!
    @IBOutlet weak var btnSelectProgOutlet: UIButton!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var btnBackOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialise()
        self.createBottomView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialise(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.viwCustom.layer.cornerRadius = self.viwCustom.frame.size.height/2
        self.viwCustom.clipsToBounds = true
        
        self.viwLeaderboard.layer.borderColor = UIColor.lightGray.cgColor
        self.viwLeaderboard.layer.borderWidth = 1
        self.viwLeaderboard.layer.cornerRadius = 5
        self.viwLeaderboard.clipsToBounds = true
        
        //iCaro
        pageControl.numberOfPages = 1
        pageControl.hidesForSinglePage = true
        viwCarousel.isPagingEnabled = true
        viwCarousel.bounces = false
        viwCarousel.delegate = self
        viwCarousel.dataSource = self
        viwCarousel.reloadData()
        
        self.tblMain.tableFooterView = UIView()
        self.tblMain.dataSource = self
        self.tblMain.delegate = self
        
        self.btnSelectProgOutlet.layer.cornerRadius = 5
        self.btnSelectProgOutlet.clipsToBounds = true
        self.btnSelectProgOutlet.layer.borderColor = UIColor.black.cgColor
        self.btnSelectProgOutlet.layer.borderWidth  = 1
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
    }
    
    
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UITableViewDatasource method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! MyDocumentListTableViewCell
        
        //Style
        cell.selectionStyle = .none
        
        cell.viwOuter.layer.cornerRadius = 5
        cell.viwOuter.clipsToBounds = true
        cell.viwOuter.layer.borderWidth = 1
        cell.viwOuter.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.imgBadge.image = UIImage(named: self.badgeArray[indexPath.row])
        circularImageView(imgViw: cell.imgBadge)
        cell.imgBadge.layer.borderColor = UIColor.white.cgColor
        cell.imgBadge.layer.borderWidth = 2
        
        cell.lblTitle.text = self.titleArray[indexPath.row]
        cell.lblDate.text = self.completionDateArray[indexPath.row]
        
        
        cell.viwCustom.layer.cornerRadius = cell.viwCustom.frame.size.height/2
        cell.viwCustom.clipsToBounds = true
        cell.viwCustom.layoutIfNeeded()
        
        
        cell.btnView.layer.cornerRadius = 5
        cell.btnView.clipsToBounds = true
        cell.btnView.addTarget(self, action: #selector(handleShare(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    
    //MARK:- Carousel Delegate
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return leaderBoardNameArray.count
    }
    
    //show the card at the particular index
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let viwCard = (Bundle.main.loadNibNamed("LeaderBoardView", owner: self, options: nil)![0] as? UIView)! as! LeaderBoardView
        viwCard.frame = viwCarousel.frame
        
        viwCard.imgBadge.layer.cornerRadius = viwCard.imgBadge.frame.size.width/2
        viwCard.imgBadge.clipsToBounds = true
        
        viwCard.imgProfile.layer.cornerRadius = viwCard.imgProfile.frame.size.width/2
        viwCard.imgProfile.clipsToBounds = true
        
        viwCard.lblName.text = self.leaderBoardNameArray[index]
        viwCard.lblTitle.text = self.leaderBoardBadgeArray[index]
        
        viwCard.imgBadge.image = UIImage(named: self.leaderBoardBadgeImageArray[index])
        viwCard.imgProfile.image = UIImage(named: "profile")
        
        
        return viwCard
    }
    //For spacing of two items
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if(carousel == viwCarousel)
        {
            if (option == .spacing) {
                return value * 1.1
            }
            return value
        }
        else
        {
            if (option == .spacing) {
                return value * 1.0
            }
            return value
        }
        
    }
    
    //scrolling started
    func carouselDidScroll(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        print("index:\(index)")
        pageControl.currentPage = index
    }
    
    //on select of specific item
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    //scroll end
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
    }
    
    
    //TODO: - UIButton Action
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Home display")
            break;
        case 1002:
             showAlert(strMSG: "Search display")
            break;
        case 1003:
            //let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
            //helpVC.isMenuNavigation = false
            //self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
            displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }
    
    
    @objc func handleShare(_ sender: UIButton){
        displayShareSheet(strBtnTitle:"Share")
    }

    
    @IBAction func btnSelectProgClick(_ sender: UIButton) {
        dropDown.anchorView = lblDDProgram
        dropDown.dataSource = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectProgOutlet.setTitle(item, for: .normal)
            print("Selected item: \(item) at index: \(index)")
        }
        dropDown.show()
    }
    @IBAction func btnBackClick(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
}
