//
//  BaseVC.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 22/04/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //TODO: - Common Alerts
    func showAlert(strMSG:String){
        let alert = UIAlertController(title: APP_NAME, message: strMSG, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (value) in
            //self.dismiss(animated: true, completion: nil), // Uncomment code on logout functionality
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //This function is used to display actionsheet along with title and msg
    func displayShareSheet(strBtnTitle:String){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: strBtnTitle, style: UIAlertActionStyle.default, handler: { (value) in
            //self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:- Set Gradient color to view
    func setGradientBackground(viw : UIView) {
        let colorBottom =  UIColor(red: 183.0/255.0, green: 39.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 3.0/255.0, green: 78.0/255.0, blue: 162.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = viw.bounds
        
        
        
        viw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    // label text color method
    func lblTextColor(label : UILabel, textColor : UIColor) {
        
        label.textColor = textColor
    }
    
    func setGradientBackgroundNew(viw : UIView) {
        let colorBottom = UIColor(red: 3.0/255.0, green: 78.0/255.0, blue: 162.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 183.0/255.0, green: 39.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = viw.bounds
        
        
        
        viw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    //save profile details in dictionary
    func save(dictionary: String) {
        let archiver = NSKeyedArchiver.archivedData(withRootObject: dictionary)
        UserDefaults.standard.set(archiver, forKey: "ProfileDataDetails")
    }
    
    func retrieveDictionary() -> String? {
        
        // Check if data exists
        guard let data = UserDefaults.standard.object(forKey: "ProfileDataDetails") else {
            return nil
        }
        
        // Check if retrieved data has correct type
        guard let retrievedData = data as? Data else {
            return nil
        }
        
        // Unarchive data
        let unarchivedObject = NSKeyedUnarchiver.unarchiveObject(with: retrievedData)
        return unarchivedObject as? String
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
   
    func circularImageView(imgViw:UIImageView)
    {
        imgViw.layer.cornerRadius = imgViw.frame.size.width / 2;
        imgViw.clipsToBounds = true
        imgViw.layer.borderWidth = 1.0
        imgViw.layer.borderColor = UIColor.clear.cgColor
    }
    
    func circularButton(button:UIButton)
    {
        button.layer.cornerRadius = button.frame.size.height / 2;
        button.clipsToBounds = true
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.clear.cgColor
    }
    func circularLabel(label:UILabel)
    {
        label.layer.cornerRadius = label.frame.size.height / 2;
        label.clipsToBounds = true
        label.layer.borderWidth = 1.0
        label.layer.borderColor = UIColor.clear.cgColor
    }
    func circularTextfield(textfield:UITextField)
    {
        textfield.layer.cornerRadius = textfield.frame.size.height / 2;
        textfield.clipsToBounds = true
        textfield.layer.borderWidth = 1.0
        textfield.layer.borderColor = UIColor(hexString: menuBgColor)?.cgColor
    }
    
    func roundedCornerView(viw:UIView,clr:String)
    {
        viw.layer.cornerRadius = 5
        viw.clipsToBounds = true
        viw.layer.borderWidth = 1.0
        viw.layer.borderColor = UIColor(hexString: clr)?.cgColor
    }
    
    func roundedCornerLabel(lbl:UILabel)
    {
        lbl.layer.cornerRadius = 5
        lbl.clipsToBounds = true
        
    }
    
    func roundedCornerButton(button:UIButton,corner:CGFloat,clr:String)
    {
        button.layer.cornerRadius = corner
        button.clipsToBounds = true
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor(hexString: clr)?.cgColor
    }
    
    
    //Dismiss Keyboard
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    //statusbar background color
    func statusBarColor()
    {
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .default
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor))
        {
            statusBar.backgroundColor = UIColor.black
        }
    }
    
    
    
   
    func showAlertWithDismis(strMSG:String){
        let alert = UIAlertController(title: APP_NAME, message: strMSG, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (value) in
            self.dismiss(animated: true, completion: nil)
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(strMessage : String)
    {
        let alertController = UIAlertController(title: APP_NAME, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed OK")
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendBackAlert(strMessage : String)
    {
        let alertController = UIAlertController(title: APP_NAME, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed OK")
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //valid email address
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    
    
    func btnBackgroundAndTextColor(button : UIButton,backgroundColor : UIColor, textColor : UIColor) {
        
        button.backgroundColor = backgroundColor
        button.setTitleColor(textColor, for: .normal)
        button.clipsToBounds = true
    }
    
    
   
    //TODO: - Custom function
    //This function is used to show notification view controller from requested viewcontroller
    func displayNotificationVC(){
        if self.navigationController != nil{
            let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "idNotificationsViewController") as! NotificationsViewController
            self.navigationController?.pushViewController(notificationVC, animated: true)
            
        }else{
            let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "idNotificationNav") as! UINavigationController
            self.present(notificationVC, animated: true, completion: nil)
        }
    }
    
    
    func displaySettingVC(){
        if self.navigationController != nil{
            let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "idSettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(notificationVC, animated: true)
            
        }
    }
   
    
    func removeSpecialCharsFromString(_ str: String) -> String {
        struct Constants {
            static let validChars = Set("1234567890".characters)
        }
        return String(str.characters.filter { Constants.validChars.contains($0) })
    }
    
    func LettersFromString(str:String)->Bool
    {
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
        
        if(str.rangeOfCharacter(from: set.inverted) != nil ){
            
            return false
        } else {
            return true
        }
    }
    
   

}
