//
//  HelpdeskViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 02/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class FAQTableViewCell : UITableViewCell{
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
}

class HelpdeskViewController: BaseVC, UITableViewDataSource, UITableViewDelegate {

    //TODO: - General
    var isLoginNavigation : Bool = Bool()
    var isMenuNavigation : Bool = Bool()
    private var descCellExpand : Bool = false
    private var expandCellIndex : Int = Int()
    
    var arrExpandCell : [Bool] = [false,false,false,false,false,false,false,false,false,false]
    
    
    //TODO: - Controls
    
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    
    @IBOutlet weak var bottomConstraintConstant: NSLayoutConstraint!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var viwHeader: UIView!
    @IBOutlet weak var btnMenuOutlet: UIButton!
    
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblMain.tableFooterView = UIView()
        self.createBottomView()
        self.initialise()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if isMenuNavigation{
            if self.revealViewController() != nil {
                btnMenuOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }else{
            self.btnMenuOutlet.setImage(UIImage(named: "nav_Back_Arrow"), for: .normal)
            self.btnMenuOutlet.addTarget(self, action: #selector(self.btnBackPress(_:)), for: .touchUpInside)
            
        }
        
        
    }
    
    
    func initialise(){
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - UITableViewDatasource Method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrExpandCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! FAQTableViewCell
        
        cell.selectionStyle = .none
        
        cell.outerView.layer.cornerRadius = 5
        cell.outerView.layer.borderWidth = 1
        cell.outerView.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        cell.outerView.clipsToBounds = true
        
        cell.titleView.layer.borderColor = UIColor(hexString: lightGrayColor)?.cgColor
        //cell.titleView.layer.borderWidth = 0
        
        print("self.arrExpandCell[indexPath.row]:\(self.arrExpandCell[indexPath.row])")
        if self.arrExpandCell[indexPath.row] == true{
            cell.imgArrow.image = UIImage(named: "up-button")
            //cell.titleView.layer.borderWidth = 1
        }else{
            cell.titleView.layer.borderColor = UIColor.black.cgColor
            //cell.titleView.layer.borderWidth = 1
            cell.imgArrow.image = UIImage(named: "down-button")
        }
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*if descCellExpand{
         descCellExpand = false
         }else{
         descCellExpand = true
         }*/
        for ind in 0...self.arrExpandCell.count-1{
            if ind == indexPath.row{
                self.arrExpandCell[indexPath.row] = true
            }else{
                self.arrExpandCell[ind] = false
            }
            
        }
        
        //expandCellIndex = indexPath.row
      //  let indexPathRow:Int = 0
        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        self.tblMain.reloadRows(at: [indexPosition], with: UITableViewRowAnimation.none)
       // self.tblMain.reloadRows(at: [indexPosition], with: .none)

        
        self.tblMain.beginUpdates()
        self.tblMain.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrExpandCell[indexPath.row] == true{
            //if descCellExpand{
            return 160
            //}else{
            //    return 60
            //}
        }else{
            return 48
        }
        
    }
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    
    //TODO: - UIButton Action
    @objc func btnBackPress(_ sender:UIButton){
        if self.navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    //This method is use to show bottom button are pressed
    //1001: Home, 1002: Search, 1003: Assistance, 1004: Setting
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Home display")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1004:
           displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    //TODO: - UIButton Action
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
        default:
            break;
        }
    }


}
