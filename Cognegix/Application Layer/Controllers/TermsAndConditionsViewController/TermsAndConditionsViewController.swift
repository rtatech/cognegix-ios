//
//  TermsAndConditionsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: BaseVC , UIWebViewDelegate{

    
    //TODO: - General
    
    //TODO: - Controls
    
    
    @IBOutlet weak var viwWeb: UIWebView!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        let url = URL (string: "https://termsfeed.com/blog/sample-terms-and-conditions-template/")
        let requestObj = URLRequest(url: url!)
        viwWeb.delegate = self
        viwWeb.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: - UIWebView Delegate Method implemenation
    func webViewDidStartLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
   
    func webViewDidFinishLoad(_ webView: UIWebView){
         UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    //TODO: - UIButton Action
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
