//
//  ForgotPasswordFirstViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ForgotPasswordFirstViewController: BaseVC {

    
    
    //TODO: - General
    
    
    //TODO: - Controls
    
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialization()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularTextfield(textfield: self.txtEmailId)
        circularButton(button: self.btnSubmitOutlet)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
       self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Left padding
        self.txtEmailId.setLeftPaddingPoints(20)
        //Right padding
        self.txtEmailId.setRightPaddingPoints(8)
       
    }

    //TODO: - UIButton Action

    @IBAction func btnSubmitClick(_ sender: UIButton) {
        let forgotSecVC = self.storyboard?.instantiateViewController(withIdentifier: "idForgotPasswordSecondViewController")  as! ForgotPasswordSecondViewController
        self.present(forgotSecVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
}
