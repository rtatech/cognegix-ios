//
//  ForgotPasswordSecondViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class ForgotPasswordSecondViewController: BaseVC {

     //TODO: - General
    
     //TODO: - Controls
    
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    override func viewDidLoad() {
        super.viewDidLoad()

         self.initialization()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularTextfield(textfield: self.txtOTP)
         circularTextfield(textfield: self.txtNewPassword)
         circularTextfield(textfield: self.txtConfPassword)
        circularButton(button: self.btnSubmitOutlet)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        //Left padding
        self.txtOTP.setLeftPaddingPoints(20)
        self.txtNewPassword.setLeftPaddingPoints(20)
        self.txtConfPassword.setLeftPaddingPoints(20)
        
        //Right padding
        self.txtOTP.setRightPaddingPoints(8)
         self.txtNewPassword.setRightPaddingPoints(8)
         self.txtConfPassword.setRightPaddingPoints(8)
    }
    
    //TODO: - UIButton Action

    @IBAction func btnSubmitClick(_ sender: UIButton) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)

//        let tempVc = self.storyboard?.instantiateViewController(withIdentifier: "idRevelBase")  as! UINavigationController
        //self.present(tempVc, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
