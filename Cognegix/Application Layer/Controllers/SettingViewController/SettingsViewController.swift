//
//  SettingsViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 10/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class SettingsViewController: BaseVC {

    //TODO: - General
    
    
    //TODO: - Controls
    
    @IBOutlet weak var btnChangePwdOutlet: UIButton!
    @IBOutlet weak var txtConfPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var viwChangePwd: UIView!
    @IBOutlet weak var viwProfile: UIView!
    @IBOutlet weak var btnNotificationOutlet: UIButton!
    @IBOutlet weak var btnProfileIcon: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialization()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularButton(button: self.btnProfileIcon)
        circularButton(button: self.btnChangePwdOutlet)
        roundedCornerView(viw: self.viwProfile, clr: lightGrayColor)
        roundedCornerView(viw: self.viwChangePwd, clr: lightGrayColor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TODO: - Custom Function
    func initialization(){
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        
    }
    
    
    //TODO: - UIButton Action

    @IBAction func btnChangePwdClick(_ sender: UIButton) {
        showAlert(strMSG: "Password changed")
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
