//
//  PastProgramViewController.swift
//  Cognegix
//
//  Created by Suraj Sonawane on 08/05/18.
//  Copyright © 2018 Suraj Sonawane. All rights reserved.
//

import UIKit

class PastProgramTableViewCell : UITableViewCell{
    
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblCompletionDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblIcon: UILabel!
    @IBOutlet weak var viwOuter: UIView!
}

class PastProgramViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {

    
    //TODO: - General
    var titleArray = ["Understanding of Negotiation Skills","Understanding negotiation situation","Grievance handling","Performance Appraisal handling"]
    var programImageArray : [String] = ["temp_program_screen.png","temp_program_screen1.jpg","temp_program_screen2.jpg","temp_program_screen3.jpg"]
    
    
    //TODO: - Controls
    
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var bottomView: BottomViewClass!
    @IBOutlet weak var menuBtnOutlet: UIButton!
    @IBOutlet weak var viwHeader: UIView!
    //TODO: - Let's Code
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Bottom Menu Navigation
        self.createBottomView()
        self.bottomView.backgroundColor = UIColor(hexString: menuBgColor)
        
        circularButton(button: self.btnProfileImage)
        circularLabel(label: self.lblNotificationCount)
        self.lblNotificationCount.backgroundColor = UIColor(hexString: NotificationCountBG)
        
        self.viwHeader.backgroundColor = UIColor(hexString: menuBgColor)
        self.tblMain.tableFooterView = UIView()
        self.tblMain.delegate = self
        self.tblMain.dataSource = self
        
        //Revealviewcontroller code
        self.navigationController?.isNavigationBarHidden = true
        if self.revealViewController() != nil {
            menuBtnOutlet.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //TODO: - UTableViewDatasource method implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return titleArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! PastProgramTableViewCell
        cell.selectionStyle = .none
        
        cell.lblIcon.layer.cornerRadius = cell.lblIcon.frame.size.width/2
        cell.lblIcon.clipsToBounds = true
        
        roundedCornerView(viw: cell.viwOuter, clr: lightGrayColor)
        roundedCornerButton(button: cell.btnView, corner: 5, clr: btnColor)
        cell.lblCompletionDate.text = "Completion Data: 21 Jan 2018"
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(btnGetCVVPressed), for: .touchUpInside)
        cell.lblTitle.text = self.titleArray[indexPath.row]
        cell.lblIcon.text = "UN"
        return cell
    }
    
    
    //This function is use to create bottom view
    func createBottomView(){
        bottomView.btnHome.tag = 1001
        bottomView.btnHome.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSearch.tag = 1002
        bottomView.btnSearch.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnHelpDesk.tag = 1003
        bottomView.btnHelpDesk.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
        
        bottomView.btnSetting.tag = 1004
        bottomView.btnSetting.addTarget(self, action: #selector(self.bottomButtonPress(_:)), for: .touchUpInside)
    }
    
    //TODO: - UIButton Action
    @objc func bottomButtonPress(_ sender:UIButton){
        print(sender.tag)
        let tag = sender.tag
        switch tag {
        case 1001:
            showAlert(strMSG: "Show home")
            break;
        case 1002:
            showAlert(strMSG: "Search display")
            break;
        case 1003:
            //let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "idFAQViewController") as! FAQViewController
            // helpVC.isMenuNavigation = false
            // self.navigationController?.pushViewController(helpVC, animated: true)
            break;
        case 1004:
           displaySettingVC()
            break;
        default:
            break;
        }
    }
    
    
    //TODO: - UIButton Action
    
    @objc func btnGetCVVPressed(_ sender: UIButton) {
        let progDtVC = self.storyboard?.instantiateViewController(withIdentifier: "idProgramDetailsViewController") as! ProgramDetailsViewController
        Singleton.shared.progDetailImageString = self.programImageArray[sender.tag]
        Singleton.shared.progDetailTitleString = self.titleArray[sender.tag]
        self.navigationController?.pushViewController(progDtVC, animated: true)
    }
    
    //31: Notification, 32: Profile
    @IBAction func btnHeaderAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 31:
            displayNotificationVC()
            break;
        case 32:
            break;
            
        default:
            break;
        }
    }

}
